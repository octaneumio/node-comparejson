var source = require('./source.json');
var target = require('./target.json');

loadlogger();
checkJson();

function loadlogger() {
    var myLogModule = require('./log.js');
}

function checkJson() {
      for(let i = 0; i<source.length;i++) {

          // Counter to check if match not found in target json string
          var checkCounter = 0;
          for(let j=0;j<target.length;j++){
            if(JSON.stringify(source[i]) ===  JSON.stringify(target[j])) {
                console.log("\n Match found for "+JSON.stringify(source[i])+
                    " \n Source element position ", i,
                    "\n Target element position ",  j);
            } else {
                checkCounter = checkCounter + 1;
            }
          }

          if(checkCounter === target.length) {
            console.log("\n Match not found for "+JSON.stringify(source[i]));
          }
      }
}



